﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen_1.Classes
{
    public enum ColorOfVehicle
    {
        RED,
        GREEN,
        BLUE,
        BLACK,
        WHITE,
        AERO,
        ANARANTHPINK,
        CANARYYELLOW,
        BRONZE,
        FUCHSIAPINK
    }

    public enum BrandOfVehicle
    {
        Toyota,
        Hyundai,
        Honda,
        Mitsubishi,
        MercedesBenz,
        volkswagen,
        suzuki,
        peugeot,
        kia,
        Mazda,
        chevrolet,
        scania,
        mack,
        volvo

    }


    public enum TypeOfTrunck
    {
        BallastTractor,
        ConcreteTransportTrunck,
        CraneTrunck,
        DumpTrunck,
        GarbageTrunck,
        LogCarrier,
        RefrigeratorTrunck,
        SeniTrailer,
        TankTrunck
    }


    public enum TypeOfVehicle
    {
        Auto,
        Bus,
        Truck
    }

    public enum TypeOfAutomovile
    {
        SUV,
        Sedan,
        Hatchback,
        Pickup,
        Coupe

    }

    public enum TypeOfService
    {
        Tourism,
        PublicTransport,
        TransportingOfStudent

    }

}
