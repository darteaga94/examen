﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen_1.Classes
{
   abstract class Vehicle
    {

       protected String _registration;
       protected int _model;
       protected  BrandOfVehicle _brand;
       protected  ColorOfVehicle _color;
       protected int _passengercapacity;
       protected double _fiscalvalue;


       public Vehicle(String registration, int model, BrandOfVehicle brand, ColorOfVehicle color,
           int passengercapacity, double _fiscalvalue)
       {
           this._registration = registration;
           this._model = model;
           this._brand = brand;
           this._color = color;
           this._passengercapacity = passengercapacity;
           this._fiscalvalue = _fiscalvalue;
       
       }


       public abstract double CalculateMandatoryInsurance();










    }
}
