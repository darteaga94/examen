﻿namespace Examen_1
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbResults = new System.Windows.Forms.GroupBox();
            this.txtSeguroPagar = new System.Windows.Forms.TextBox();
            this.lbNetEarnings = new System.Windows.Forms.Label();
            this.gbAboutEmployee = new System.Windows.Forms.GroupBox();
            this.txtValorFiscal = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbTypeOfColor = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbTypeOfMarca = new System.Windows.Forms.ComboBox();
            this.btCalculate = new System.Windows.Forms.Button();
            this.btClear = new System.Windows.Forms.Button();
            this.gbCamion = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cmbTypeOfCamion = new System.Windows.Forms.ComboBox();
            this.txtNumeroEjes = new System.Windows.Forms.TextBox();
            this.lbOvertime = new System.Windows.Forms.Label();
            this.gbAutobus = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cmbTypeOfServicio = new System.Windows.Forms.ComboBox();
            this.txtCantidadAsientos = new System.Windows.Forms.TextBox();
            this.lbAmountOfSales = new System.Windows.Forms.Label();
            this.gbAutomovil = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbTypeOfAutomóvil = new System.Windows.Forms.ComboBox();
            this.txtnumerosPuertas = new System.Windows.Forms.TextBox();
            this.lbPartsProduced = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbTypeOfVehicle = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCapacidad = new System.Windows.Forms.TextBox();
            this.txtAnno_modelo = new System.Windows.Forms.TextBox();
            this.txtPlaca = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panelHeader = new System.Windows.Forms.Panel();
            this.lbFormTitle = new System.Windows.Forms.Label();
            this.gbResults.SuspendLayout();
            this.gbAboutEmployee.SuspendLayout();
            this.gbCamion.SuspendLayout();
            this.gbAutobus.SuspendLayout();
            this.gbAutomovil.SuspendLayout();
            this.panelHeader.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbResults
            // 
            this.gbResults.Controls.Add(this.txtSeguroPagar);
            this.gbResults.Controls.Add(this.lbNetEarnings);
            this.gbResults.Location = new System.Drawing.Point(19, 440);
            this.gbResults.Name = "gbResults";
            this.gbResults.Size = new System.Drawing.Size(666, 86);
            this.gbResults.TabIndex = 5;
            this.gbResults.TabStop = false;
            this.gbResults.Text = "Resultado";
            // 
            // txtSeguroPagar
            // 
            this.txtSeguroPagar.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSeguroPagar.Location = new System.Drawing.Point(183, 28);
            this.txtSeguroPagar.Name = "txtSeguroPagar";
            this.txtSeguroPagar.ReadOnly = true;
            this.txtSeguroPagar.Size = new System.Drawing.Size(452, 35);
            this.txtSeguroPagar.TabIndex = 1;
            // 
            // lbNetEarnings
            // 
            this.lbNetEarnings.AutoSize = true;
            this.lbNetEarnings.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNetEarnings.Location = new System.Drawing.Point(22, 34);
            this.lbNetEarnings.Name = "lbNetEarnings";
            this.lbNetEarnings.Size = new System.Drawing.Size(162, 25);
            this.lbNetEarnings.TabIndex = 0;
            this.lbNetEarnings.Text = "Seguro a Pagar";
            // 
            // gbAboutEmployee
            // 
            this.gbAboutEmployee.Controls.Add(this.txtValorFiscal);
            this.gbAboutEmployee.Controls.Add(this.label7);
            this.gbAboutEmployee.Controls.Add(this.cmbTypeOfColor);
            this.gbAboutEmployee.Controls.Add(this.label6);
            this.gbAboutEmployee.Controls.Add(this.cmbTypeOfMarca);
            this.gbAboutEmployee.Controls.Add(this.btCalculate);
            this.gbAboutEmployee.Controls.Add(this.btClear);
            this.gbAboutEmployee.Controls.Add(this.gbCamion);
            this.gbAboutEmployee.Controls.Add(this.gbAutobus);
            this.gbAboutEmployee.Controls.Add(this.gbAutomovil);
            this.gbAboutEmployee.Controls.Add(this.label5);
            this.gbAboutEmployee.Controls.Add(this.cmbTypeOfVehicle);
            this.gbAboutEmployee.Controls.Add(this.label4);
            this.gbAboutEmployee.Controls.Add(this.label3);
            this.gbAboutEmployee.Controls.Add(this.txtCapacidad);
            this.gbAboutEmployee.Controls.Add(this.txtAnno_modelo);
            this.gbAboutEmployee.Controls.Add(this.txtPlaca);
            this.gbAboutEmployee.Controls.Add(this.label2);
            this.gbAboutEmployee.Controls.Add(this.label1);
            this.gbAboutEmployee.Location = new System.Drawing.Point(19, 80);
            this.gbAboutEmployee.Name = "gbAboutEmployee";
            this.gbAboutEmployee.Size = new System.Drawing.Size(666, 345);
            this.gbAboutEmployee.TabIndex = 4;
            this.gbAboutEmployee.TabStop = false;
            this.gbAboutEmployee.Text = "Datos del empleado";
            // 
            // txtValorFiscal
            // 
            this.txtValorFiscal.Location = new System.Drawing.Point(120, 214);
            this.txtValorFiscal.Name = "txtValorFiscal";
            this.txtValorFiscal.Size = new System.Drawing.Size(204, 20);
            this.txtValorFiscal.TabIndex = 19;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 148);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Color";
            // 
            // cmbTypeOfColor
            // 
            this.cmbTypeOfColor.FormattingEnabled = true;
            this.cmbTypeOfColor.Location = new System.Drawing.Point(120, 145);
            this.cmbTypeOfColor.Name = "cmbTypeOfColor";
            this.cmbTypeOfColor.Size = new System.Drawing.Size(204, 21);
            this.cmbTypeOfColor.TabIndex = 18;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(18, 112);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Marca";
            // 
            // cmbTypeOfMarca
            // 
            this.cmbTypeOfMarca.FormattingEnabled = true;
            this.cmbTypeOfMarca.Location = new System.Drawing.Point(120, 109);
            this.cmbTypeOfMarca.Name = "cmbTypeOfMarca";
            this.cmbTypeOfMarca.Size = new System.Drawing.Size(204, 21);
            this.cmbTypeOfMarca.TabIndex = 16;
            // 
            // btCalculate
            // 
            this.btCalculate.Location = new System.Drawing.Point(120, 246);
            this.btCalculate.Name = "btCalculate";
            this.btCalculate.Size = new System.Drawing.Size(100, 80);
            this.btCalculate.TabIndex = 13;
            this.btCalculate.Text = "Calcular";
            this.btCalculate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btCalculate.UseVisualStyleBackColor = true;
            this.btCalculate.Click += new System.EventHandler(this.btCalculate_Click);
            // 
            // btClear
            // 
            this.btClear.Location = new System.Drawing.Point(226, 246);
            this.btClear.Name = "btClear";
            this.btClear.Size = new System.Drawing.Size(100, 80);
            this.btClear.TabIndex = 14;
            this.btClear.Text = "Limpiar";
            this.btClear.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btClear.UseVisualStyleBackColor = true;
            // 
            // gbCamion
            // 
            this.gbCamion.Controls.Add(this.label10);
            this.gbCamion.Controls.Add(this.cmbTypeOfCamion);
            this.gbCamion.Controls.Add(this.txtNumeroEjes);
            this.gbCamion.Controls.Add(this.lbOvertime);
            this.gbCamion.Location = new System.Drawing.Point(351, 198);
            this.gbCamion.Name = "gbCamion";
            this.gbCamion.Size = new System.Drawing.Size(299, 92);
            this.gbCamion.TabIndex = 12;
            this.gbCamion.TabStop = false;
            this.gbCamion.Text = "Camion";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(19, 60);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 13);
            this.label10.TabIndex = 6;
            this.label10.Text = "Tipo de Camion";
            // 
            // cmbTypeOfCamion
            // 
            this.cmbTypeOfCamion.FormattingEnabled = true;
            this.cmbTypeOfCamion.Location = new System.Drawing.Point(146, 57);
            this.cmbTypeOfCamion.Name = "cmbTypeOfCamion";
            this.cmbTypeOfCamion.Size = new System.Drawing.Size(138, 21);
            this.cmbTypeOfCamion.TabIndex = 7;
            // 
            // txtNumeroEjes
            // 
            this.txtNumeroEjes.Location = new System.Drawing.Point(146, 19);
            this.txtNumeroEjes.Name = "txtNumeroEjes";
            this.txtNumeroEjes.Size = new System.Drawing.Size(138, 20);
            this.txtNumeroEjes.TabIndex = 1;
            // 
            // lbOvertime
            // 
            this.lbOvertime.AutoSize = true;
            this.lbOvertime.Location = new System.Drawing.Point(19, 22);
            this.lbOvertime.Name = "lbOvertime";
            this.lbOvertime.Size = new System.Drawing.Size(81, 13);
            this.lbOvertime.TabIndex = 0;
            this.lbOvertime.Text = "Numero de ejes";
            // 
            // gbAutobus
            // 
            this.gbAutobus.Controls.Add(this.label9);
            this.gbAutobus.Controls.Add(this.cmbTypeOfServicio);
            this.gbAutobus.Controls.Add(this.txtCantidadAsientos);
            this.gbAutobus.Controls.Add(this.lbAmountOfSales);
            this.gbAutobus.Location = new System.Drawing.Point(351, 112);
            this.gbAutobus.Name = "gbAutobus";
            this.gbAutobus.Size = new System.Drawing.Size(299, 80);
            this.gbAutobus.TabIndex = 11;
            this.gbAutobus.TabStop = false;
            this.gbAutobus.Text = "Autobus";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(19, 52);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(84, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "Tipo de Servicio";
            // 
            // cmbTypeOfServicio
            // 
            this.cmbTypeOfServicio.FormattingEnabled = true;
            this.cmbTypeOfServicio.Location = new System.Drawing.Point(146, 49);
            this.cmbTypeOfServicio.Name = "cmbTypeOfServicio";
            this.cmbTypeOfServicio.Size = new System.Drawing.Size(138, 21);
            this.cmbTypeOfServicio.TabIndex = 5;
            // 
            // txtCantidadAsientos
            // 
            this.txtCantidadAsientos.Location = new System.Drawing.Point(146, 19);
            this.txtCantidadAsientos.Name = "txtCantidadAsientos";
            this.txtCantidadAsientos.Size = new System.Drawing.Size(138, 20);
            this.txtCantidadAsientos.TabIndex = 1;
            // 
            // lbAmountOfSales
            // 
            this.lbAmountOfSales.AutoSize = true;
            this.lbAmountOfSales.Location = new System.Drawing.Point(19, 22);
            this.lbAmountOfSales.Name = "lbAmountOfSales";
            this.lbAmountOfSales.Size = new System.Drawing.Size(107, 13);
            this.lbAmountOfSales.TabIndex = 0;
            this.lbAmountOfSales.Text = "Cantidad de Asientos";
            // 
            // gbAutomovil
            // 
            this.gbAutomovil.Controls.Add(this.label8);
            this.gbAutomovil.Controls.Add(this.cmbTypeOfAutomóvil);
            this.gbAutomovil.Controls.Add(this.txtnumerosPuertas);
            this.gbAutomovil.Controls.Add(this.lbPartsProduced);
            this.gbAutomovil.Location = new System.Drawing.Point(351, 26);
            this.gbAutomovil.Name = "gbAutomovil";
            this.gbAutomovil.Size = new System.Drawing.Size(299, 80);
            this.gbAutomovil.TabIndex = 10;
            this.gbAutomovil.TabStop = false;
            this.gbAutomovil.Text = "Automóvil";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(19, 53);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(92, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Tipo de Automóvil";
            // 
            // cmbTypeOfAutomóvil
            // 
            this.cmbTypeOfAutomóvil.FormattingEnabled = true;
            this.cmbTypeOfAutomóvil.Location = new System.Drawing.Point(134, 50);
            this.cmbTypeOfAutomóvil.Name = "cmbTypeOfAutomóvil";
            this.cmbTypeOfAutomóvil.Size = new System.Drawing.Size(150, 21);
            this.cmbTypeOfAutomóvil.TabIndex = 3;
            // 
            // txtnumerosPuertas
            // 
            this.txtnumerosPuertas.Location = new System.Drawing.Point(134, 19);
            this.txtnumerosPuertas.Name = "txtnumerosPuertas";
            this.txtnumerosPuertas.Size = new System.Drawing.Size(150, 20);
            this.txtnumerosPuertas.TabIndex = 1;
            // 
            // lbPartsProduced
            // 
            this.lbPartsProduced.AutoSize = true;
            this.lbPartsProduced.Location = new System.Drawing.Point(19, 22);
            this.lbPartsProduced.Name = "lbPartsProduced";
            this.lbPartsProduced.Size = new System.Drawing.Size(98, 13);
            this.lbPartsProduced.TabIndex = 0;
            this.lbPartsProduced.Text = "Numero de Puertas";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Tipo de Vehiculo";
            // 
            // cmbTypeOfVehicle
            // 
            this.cmbTypeOfVehicle.FormattingEnabled = true;
            this.cmbTypeOfVehicle.Location = new System.Drawing.Point(120, 26);
            this.cmbTypeOfVehicle.Name = "cmbTypeOfVehicle";
            this.cmbTypeOfVehicle.Size = new System.Drawing.Size(204, 21);
            this.cmbTypeOfVehicle.TabIndex = 1;
            this.cmbTypeOfVehicle.SelectedValueChanged += new System.EventHandler(this.cmbTypeOfVehicle_SelectedValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 217);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Valor Fiscal";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 179);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Capacidad";
            // 
            // txtCapacidad
            // 
            this.txtCapacidad.Location = new System.Drawing.Point(120, 176);
            this.txtCapacidad.Name = "txtCapacidad";
            this.txtCapacidad.Size = new System.Drawing.Size(204, 20);
            this.txtCapacidad.TabIndex = 7;
            // 
            // txtAnno_modelo
            // 
            this.txtAnno_modelo.Location = new System.Drawing.Point(120, 79);
            this.txtAnno_modelo.Name = "txtAnno_modelo";
            this.txtAnno_modelo.Size = new System.Drawing.Size(204, 20);
            this.txtAnno_modelo.TabIndex = 5;
            // 
            // txtPlaca
            // 
            this.txtPlaca.Location = new System.Drawing.Point(120, 53);
            this.txtPlaca.Name = "txtPlaca";
            this.txtPlaca.Size = new System.Drawing.Size(204, 20);
            this.txtPlaca.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Año Modelo";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Placa";
            // 
            // panelHeader
            // 
            this.panelHeader.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panelHeader.Controls.Add(this.lbFormTitle);
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader.Location = new System.Drawing.Point(0, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(881, 59);
            this.panelHeader.TabIndex = 3;
            // 
            // lbFormTitle
            // 
            this.lbFormTitle.AutoSize = true;
            this.lbFormTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFormTitle.Location = new System.Drawing.Point(12, 9);
            this.lbFormTitle.Name = "lbFormTitle";
            this.lbFormTitle.Size = new System.Drawing.Size(536, 39);
            this.lbFormTitle.TabIndex = 0;
            this.lbFormTitle.Text = "“Seguro Obligatorio de Vehículos”";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(881, 607);
            this.Controls.Add(this.gbResults);
            this.Controls.Add(this.gbAboutEmployee);
            this.Controls.Add(this.panelHeader);
            this.Name = "Form1";
            this.Text = "Form1";
            this.gbResults.ResumeLayout(false);
            this.gbResults.PerformLayout();
            this.gbAboutEmployee.ResumeLayout(false);
            this.gbAboutEmployee.PerformLayout();
            this.gbCamion.ResumeLayout(false);
            this.gbCamion.PerformLayout();
            this.gbAutobus.ResumeLayout(false);
            this.gbAutobus.PerformLayout();
            this.gbAutomovil.ResumeLayout(false);
            this.gbAutomovil.PerformLayout();
            this.panelHeader.ResumeLayout(false);
            this.panelHeader.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbResults;
        private System.Windows.Forms.TextBox txtSeguroPagar;
        private System.Windows.Forms.Label lbNetEarnings;
        private System.Windows.Forms.GroupBox gbAboutEmployee;
        private System.Windows.Forms.Button btCalculate;
        private System.Windows.Forms.Button btClear;
        private System.Windows.Forms.GroupBox gbCamion;
        private System.Windows.Forms.TextBox txtNumeroEjes;
        private System.Windows.Forms.Label lbOvertime;
        private System.Windows.Forms.GroupBox gbAutobus;
        private System.Windows.Forms.TextBox txtCantidadAsientos;
        private System.Windows.Forms.Label lbAmountOfSales;
        private System.Windows.Forms.GroupBox gbAutomovil;
        private System.Windows.Forms.TextBox txtnumerosPuertas;
        private System.Windows.Forms.Label lbPartsProduced;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbTypeOfVehicle;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCapacidad;
        private System.Windows.Forms.TextBox txtAnno_modelo;
        private System.Windows.Forms.TextBox txtPlaca;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.Label lbFormTitle;
        private System.Windows.Forms.TextBox txtValorFiscal;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbTypeOfColor;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbTypeOfMarca;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbTypeOfAutomóvil;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cmbTypeOfServicio;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cmbTypeOfCamion;
    }
}

