﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Examen_1.Classes;

namespace Examen_1.Models
{
    class Truck: Vehicle
    {
        private TypeOfTrunck typeOfTruck;
        private int NumberOfAxles;

      


public Truck(String registration, int model, BrandOfVehicle brand, ColorOfVehicle color, int passengercapacity, double _fiscalvalue):
             base(registration,  model,  brand,  color,passengercapacity,  _fiscalvalue)
       {
           
       }
  
        public int NumberOfAxles1
        {
            get { return NumberOfAxles; }
            set { NumberOfAxles = value; }
        }
        
        public TypeOfTrunck TypeOfTruck
        {
            get { return typeOfTruck; }
            set { typeOfTruck = value; }
        }

        public override double CalculateMandatoryInsurance()
        {
            double MT = 0;
            double VF = this._fiscalvalue;
            int AA = Math.Max((DateTime.Today.Year - this._model), 1);
            double MC = CalculateTaxForTruck();

            switch (this.typeOfTruck)
            {
                case TypeOfTrunck.BallastTractor:
                    MT = (VF * (10/100) / AA) + MC;
                    break;
                case TypeOfTrunck.ConcreteTransportTrunck:
                    MT = (VF * (5/100) / AA) + MC;
                    break;
                case TypeOfTrunck.CraneTrunck:
                    MT = (VF * (20/100) / AA) + MC;
                    break;
                case TypeOfTrunck.DumpTrunck:
                    MT = (VF * (5 / 100) / AA) + MC;
                    break;
                case TypeOfTrunck.GarbageTrunck:
                    MT = (VF * (2/100) / AA) + MC;
                    break;
                case TypeOfTrunck.LogCarrier:
                    MT = (VF * (12/100) / AA) + MC;
                    break;
                case TypeOfTrunck.RefrigeratorTrunck:
                    MT = (VF * (15/100) / AA) + MC;
                    break;
                case TypeOfTrunck.SeniTrailer:
                     MT = (VF * (7/100) / AA) + MC;
                    break;
                case TypeOfTrunck.TankTrunck:
                     MT = (VF * (10/100) / AA) + MC;
                    break;
                default:
                    break;
            }
            return MT;

        }

        private double CalculateTaxForTruck()
        {
             double VF = this._fiscalvalue;
            return (VF/50) * (1/100) * this.NumberOfAxles;
        }


    }
}
