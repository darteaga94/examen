﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Examen_1.Classes;

namespace Examen_1.Models
{
    class Car : Vehicle
    {

        private TypeOfAutomovile typeOfAutomobile;
        private int NumberOfDoor;


        public Car(String registration, int model, BrandOfVehicle brand, ColorOfVehicle color, int passengercapacity, double _fiscalvalue):
             base(registration,  model,  brand,  color,passengercapacity,  _fiscalvalue)
       {
         
       }
  
        public TypeOfAutomovile TypeOfAutomobile
        {
            get { return typeOfAutomobile; }
            set { typeOfAutomobile = value; }
        }
 
        public int NumberOfDoor1
        {
            get { return NumberOfDoor; }
            set { NumberOfDoor = value; }
        }


        public override double CalculateMandatoryInsurance()
        {
            double MT = 0;
            double VF = this._fiscalvalue;
            int AA = Math.Max((DateTime.Today.Year - this._model), 1);
            double MC = CalculateTaxForCar();

            switch (this.typeOfAutomobile)
            {
                case TypeOfAutomovile.SUV:
                     MT = (VF * (10/100) / AA) + MC;
                    break;
                case TypeOfAutomovile.Sedan:
                    MT = (VF * (5 / 100) / AA) + MC;
                    break;
                case TypeOfAutomovile.Hatchback:
                    MT =  (VF * (7/100) / AA) + MC;
                    break;
                case TypeOfAutomovile.Pickup:
                     MT =(VF * (3/100) / AA) + MC;
                    break;
                case TypeOfAutomovile.Coupe:
                     MT = (VF * (5/100) / AA) + MC;
                    break;
                default:
                    break;
            }
            return MT;
        }


        private double CalculateTaxForCar()
        {
            double VF = this._fiscalvalue;
            
            return (VF/50) * (1/100) * this.NumberOfDoor;
        }



    }
}
