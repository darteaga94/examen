﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Examen_1.Classes;

namespace Examen_1.Models
{
    class Bus : Vehicle
    {

        private TypeOfService typeOfService;
        private int seatingCapacity;

       

           public Bus(String registration, int model, BrandOfVehicle brand, ColorOfVehicle color, int passengercapacity, double _fiscalvalue):
             base(registration,  model,  brand,  color,passengercapacity,  _fiscalvalue)
       {
        
       }
 

        public TypeOfService TypeOfService
        {
            get { return typeOfService; }
            set { typeOfService = value; }
        }
 
        public int SeatingCapacity
        {
            get { return seatingCapacity; }
            set { seatingCapacity = value; }
        }


        public override double CalculateMandatoryInsurance()
        {
            double MT = 0;
            double VF = this._fiscalvalue;
            int AA = Math.Max((DateTime.Today.Year - this._model), 1);
            double MC = CalculateTaxForBus();
            switch (this.typeOfService)
            {
                case TypeOfService.Tourism:
                   MT =  ((VF * (10/100)) * (AA * (5/100))) + MC;
                    break;
                case TypeOfService.PublicTransport:
                    MT =  ((VF * (5/100)) * (AA * (5/100))) + MC;

                    break;
                case TypeOfService.TransportingOfStudent:
                   MT =  ((VF * (3/100)) * (AA * (5/100))) + MC;
                    break;
                default:
                    break;
            }
            return MT;
        }

        private double CalculateTaxForBus()
        {
            double VF = this._fiscalvalue;
            return (VF/150) * (1/100) * this.seatingCapacity;
        }

    }
}
