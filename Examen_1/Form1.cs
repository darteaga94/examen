﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Examen_1.Classes;
using Examen_1.Models;

namespace Examen_1
{
    public partial class Form1 : Form
    {

        Dictionary<string, TypeOfVehicle> dictionary;
        Dictionary<string, TypeOfAutomovile> dictionary2;
        Dictionary<string, TypeOfService> dictionary3;
        Dictionary<string, TypeOfTrunck> dictionary4;
        Dictionary<string, BrandOfVehicle> dictionary5;
        Dictionary<string, ColorOfVehicle> dictionary6;


        public Form1()
        {
            InitializeComponent();


            //Se declara y llena un objeto Dictionary
            dictionary = new Dictionary<string, TypeOfVehicle>();
            dictionary.Add("Auto", TypeOfVehicle.Auto);
            dictionary.Add("Bus", TypeOfVehicle.Bus);
            dictionary.Add("Truck", TypeOfVehicle.Truck);

            //Se inhabilitan los grupos de controles
            this.gbAutomovil.Enabled = false;
            this.gbAutobus.Enabled = false;
            this.gbCamion.Enabled = false;

            //Se pega el combobox al origen de datos
            this.cmbTypeOfVehicle.DisplayMember = "Key";
            this.cmbTypeOfVehicle.ValueMember = "Value";
            this.cmbTypeOfVehicle.DataSource = new BindingSource(dictionary, null);
            this.cmbTypeOfVehicle.DropDownStyle = ComboBoxStyle.DropDownList;










            //Se declara y llena un objeto Dictionary
            dictionary2 = new Dictionary<string, TypeOfAutomovile>();
            dictionary2.Add("Coupe", TypeOfAutomovile.Coupe);
            dictionary2.Add("Hatchback", TypeOfAutomovile.Hatchback);
            dictionary2.Add("Pickup", TypeOfAutomovile.Pickup);
            dictionary2.Add("Sedan", TypeOfAutomovile.Sedan);
            dictionary2.Add("SUV", TypeOfAutomovile.SUV);

            //Se inhabilitan los grupos de controles
            this.gbAutomovil.Enabled = false;
            this.gbAutobus.Enabled = false;
            this.gbCamion.Enabled = false;

            //Se pega el combobox al origen de datos
            this.cmbTypeOfAutomóvil.DisplayMember = "Key";
            this.cmbTypeOfAutomóvil.ValueMember = "Value";
            this.cmbTypeOfAutomóvil.DataSource = new BindingSource(dictionary, null);
            this.cmbTypeOfAutomóvil.DropDownStyle = ComboBoxStyle.DropDownList;





            //Se declara y llena un objeto Dictionary
            dictionary3 = new Dictionary<string, TypeOfService>();
            dictionary3.Add("PublicTransport", TypeOfService.PublicTransport);
            dictionary3.Add("Tourism", TypeOfService.Tourism);
            dictionary3.Add("TransportingOfStudent", TypeOfService.TransportingOfStudent);

            //Se inhabilitan los grupos de controles
            this.gbAutomovil.Enabled = false;
            this.gbAutobus.Enabled = false;
            this.gbCamion.Enabled = false;

            //Se pega el combobox al origen de datos
            this.cmbTypeOfServicio.DisplayMember = "Key";
            this.cmbTypeOfServicio.ValueMember = "Value";
            this.cmbTypeOfServicio.DataSource = new BindingSource(dictionary, null);
            this.cmbTypeOfServicio.DropDownStyle = ComboBoxStyle.DropDownList;








            //Se declara y llena un objeto Dictionary
            dictionary4 = new Dictionary<string, TypeOfTrunck>();
            dictionary4.Add("BallastTractor", TypeOfTrunck.BallastTractor);
            dictionary4.Add("ConcreteTransportTrunck", TypeOfTrunck.ConcreteTransportTrunck);
            dictionary4.Add("CraneTrunck", TypeOfTrunck.CraneTrunck);
            dictionary4.Add("DumpTrunck", TypeOfTrunck.DumpTrunck);
            dictionary4.Add("GarbageTrunck", TypeOfTrunck.GarbageTrunck);
            dictionary4.Add("LogCarrier", TypeOfTrunck.LogCarrier);
            dictionary4.Add("RefrigeratorTrunck", TypeOfTrunck.RefrigeratorTrunck);
            dictionary4.Add("SeniTrailer", TypeOfTrunck.SeniTrailer);
            dictionary4.Add("TankTrunck", TypeOfTrunck.TankTrunck);

            //Se inhabilitan los grupos de controles
            this.gbAutomovil.Enabled = false;
            this.gbAutobus.Enabled = false;
            this.gbCamion.Enabled = false;

            //Se pega el combobox al origen de datos
            this.cmbTypeOfCamion.DisplayMember = "Key";
            this.cmbTypeOfCamion.ValueMember = "Value";
            this.cmbTypeOfCamion.DataSource = new BindingSource(dictionary, null);
            this.cmbTypeOfCamion.DropDownStyle = ComboBoxStyle.DropDownList;









            //Se declara y llena un objeto Dictionary
            dictionary5 = new Dictionary<string, BrandOfVehicle>();
            dictionary5.Add("chevrolet", BrandOfVehicle.chevrolet);
            dictionary5.Add("Honda", BrandOfVehicle.Honda);
            dictionary5.Add("Hyundai", BrandOfVehicle.Hyundai);
            dictionary5.Add("kia", BrandOfVehicle.kia);
            dictionary5.Add("mack", BrandOfVehicle.mack);
            dictionary5.Add("Mazda", BrandOfVehicle.Mazda);
            dictionary5.Add("MercedesBenz", BrandOfVehicle.MercedesBenz);
            dictionary5.Add("Mitsubishi", BrandOfVehicle.Mitsubishi);
            dictionary5.Add("scania", BrandOfVehicle.scania);
            dictionary5.Add("suzuki", BrandOfVehicle.suzuki);
            dictionary5.Add("Toyota", BrandOfVehicle.Toyota);
            dictionary5.Add("volkswagen", BrandOfVehicle.volkswagen);
            dictionary5.Add("volvo", BrandOfVehicle.volvo);
            

            //Se inhabilitan los grupos de controles
            this.gbAutomovil.Enabled = false;
            this.gbAutobus.Enabled = false;
            this.gbCamion.Enabled = false;

            //Se pega el combobox al origen de datos
            this.cmbTypeOfMarca.DisplayMember = "Key";
            this.cmbTypeOfMarca.ValueMember = "Value";
            this.cmbTypeOfMarca.DataSource = new BindingSource(dictionary, null);
            this.cmbTypeOfMarca.DropDownStyle = ComboBoxStyle.DropDownList;

            


            //Se declara y llena un objeto Dictionary
            dictionary6 = new Dictionary<string, ColorOfVehicle>();
            dictionary6.Add("AERO", ColorOfVehicle.AERO);
            dictionary6.Add("ANARANTHPINK", ColorOfVehicle.ANARANTHPINK);
            dictionary6.Add("BLACK", ColorOfVehicle.BLACK);
            dictionary6.Add("BLUE", ColorOfVehicle.BLUE);
            dictionary6.Add("BRONZE", ColorOfVehicle.BRONZE);
            dictionary6.Add("CANARYYELLOW", ColorOfVehicle.CANARYYELLOW);
            dictionary6.Add("FUCHSIAPINK", ColorOfVehicle.FUCHSIAPINK);
            dictionary6.Add("GREEN", ColorOfVehicle.GREEN);
            dictionary6.Add("RED", ColorOfVehicle.RED);
            dictionary6.Add("WHITE", ColorOfVehicle.WHITE);


            //Se inhabilitan los grupos de controles
            this.gbAutomovil.Enabled = false;
            this.gbAutobus.Enabled = false;
            this.gbCamion.Enabled = false;

            //Se pega el combobox al origen de datos
            this.cmbTypeOfColor.DisplayMember = "Key";
            this.cmbTypeOfColor.ValueMember = "Value";
            this.cmbTypeOfColor.DataSource = new BindingSource(dictionary, null);
            this.cmbTypeOfColor.DropDownStyle = ComboBoxStyle.DropDownList;




        }

        private void btCalculate_Click(object sender, EventArgs e)
        {
            //Declara una clase abstracta, pero se instanciará la que corresponde
            Vehicle vehicle;

            if ((cmbTypeOfVehicle.SelectedValue != null) && (cmbTypeOfMarca.SelectedValue != null) &&
                (cmbTypeOfColor.SelectedValue != null) && (txtPlaca.Text != string.Empty)
                && (txtAnno_modelo.Text != string.Empty) && (txtCapacidad.Text != string.Empty)
                && (txtValorFiscal.Text != string.Empty))
            {


                try
                {
                    //La siguiente linea provocará una excepción si el dato no es numérico
                    Double.Parse(txtValorFiscal.Text);
                    int.Parse(txtPlaca.Text);
                    int.Parse(txtAnno_modelo.Text);

                    switch ((TypeOfVehicle)cmbTypeOfVehicle.SelectedValue)
                    {
                        default:
                        case TypeOfVehicle.Auto:
                            //La siguiente linea provocará una excepción si el dato no es numérico
                            int.Parse(txtnumerosPuertas.Text);


                            vehicle = new Car(txtPlaca.Text, int.Parse(txtAnno_modelo.Text), (BrandOfVehicle)cmbTypeOfMarca.SelectedValue, (ColorOfVehicle)cmbTypeOfColor.SelectedValue,
                            int.Parse(txtCapacidad.Text),double.Parse(txtValorFiscal.Text));

                            ((Car)vehicle).TypeOfAutomobile = (TypeOfAutomovile)cmbTypeOfAutomóvil.SelectedValue;
                            ((Car)vehicle).NumberOfDoor1 = int.Parse(txtnumerosPuertas.Text);
                            break;
                        case TypeOfVehicle.Bus:
                            //La siguiente linea provocará una excepción si el dato no es numérico
                            int.Parse(txtCantidadAsientos.Text);

                            //Instancia en la variable "employee" un objeto "Seller"
                            vehicle = new Bus(txtPlaca.Text, int.Parse(txtAnno_modelo.Text), (BrandOfVehicle)cmbTypeOfMarca.SelectedValue, (ColorOfVehicle)cmbTypeOfColor.SelectedValue,
                            int.Parse(txtCapacidad.Text), double.Parse(txtValorFiscal.Text));

                            //Se hace castign a "employee" para tener acceso a los métodos propios del objeto "Seller"
                            ((Bus)vehicle).SeatingCapacity = int.Parse(txtCantidadAsientos.Text);
                            ((Bus)vehicle).TypeOfService = (TypeOfService)cmbTypeOfServicio.SelectedValue;
                            break;

                        case TypeOfVehicle.Truck:
                            //La siguiente linea provocará una excepción si el dato no es numérico
                            int.Parse(txtNumeroEjes.Text);

                            //Instancia en la variable "employee" un objeto "Administrative"
                            vehicle = new Truck(txtPlaca.Text, int.Parse(txtAnno_modelo.Text), (BrandOfVehicle)cmbTypeOfMarca.SelectedValue, (ColorOfVehicle)cmbTypeOfColor.SelectedValue,
                            int.Parse(txtCapacidad.Text), double.Parse(txtValorFiscal.Text));

                            //Se hace castign a "employee" para tener acceso a los métodos propios del objeto "Administrative"
                            ((Truck)vehicle).NumberOfAxles1 = int.Parse(txtNumeroEjes.Text);
                            ((Truck)vehicle).TypeOfTruck = (TypeOfTrunck)cmbTypeOfCamion.SelectedValue;
                            break;
                        
                    }

                    //Haber declarado el objeto base "employee" en lugar de una clase específica evitó repetir
                    //estas linea en cada sección del switch-case

                    txtSeguroPagar.Text = vehicle.CalculateMandatoryInsurance.ToString();
                }
                catch (Exception)
                {
                    MessageBox.Show("Se esperaba un valor numérico", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            } else
                MessageBox.Show("Debe llenar todos los datos de entrada","Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        

































        }

        private void cmbTypeOfVehicle_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cmbTypeOfVehicle.SelectedValue != null)
            {

                switch ((TypeOfVehicle)cmbTypeOfVehicle.SelectedValue)
                {
                    case TypeOfVehicle.Bus:
                        this.gbAutobus.Enabled = true;
                        this.gbAutomovil.Enabled = false;
                        this.gbCamion.Enabled = false;
                        break;
                    case TypeOfVehicle.Auto:
                        this.gbAutobus.Enabled = false;
                        this.gbAutomovil.Enabled = true;
                        this.gbCamion.Enabled = false;
                        break;
                    case TypeOfVehicle.Truck:
                        this.gbAutobus.Enabled = false;
                        this.gbAutomovil.Enabled = false;
                        this.gbCamion.Enabled = true;
                        break;
                }
            }
        }
    }
}

