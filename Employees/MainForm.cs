﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Employees.Views;

namespace Employees
{
    public partial class MainForm : Form
    {
        EmployeeForm employeeForm;

        public MainForm()
        {
            InitializeComponent();
        }

        private void miCalculateSalary_Click(object sender, EventArgs e)
        {
            try
            {
                if (employeeForm.IsDisposed == true)
                    employeeForm = new EmployeeForm();
                else
                    employeeForm.Activate();
            }
            catch (Exception) { employeeForm = new EmployeeForm(); }

            employeeForm.MdiParent = this;
            employeeForm.ShowInTaskbar = false;
            employeeForm.StartPosition = FormStartPosition.CenterScreen;
            employeeForm.Show();
        }

        private void miExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
