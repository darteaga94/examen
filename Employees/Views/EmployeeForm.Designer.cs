﻿namespace Employees.Views
{
    partial class EmployeeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbFormTitle = new System.Windows.Forms.Label();
            this.panelHeader = new System.Windows.Forms.Panel();
            this.gbAboutEmployee = new System.Windows.Forms.GroupBox();
            this.btCalculate = new System.Windows.Forms.Button();
            this.btClear = new System.Windows.Forms.Button();
            this.gbAdministrative = new System.Windows.Forms.GroupBox();
            this.txtOvertime = new System.Windows.Forms.TextBox();
            this.lbOvertime = new System.Windows.Forms.Label();
            this.gbSeller = new System.Windows.Forms.GroupBox();
            this.txtCommission = new System.Windows.Forms.TextBox();
            this.txtAmountOfSales = new System.Windows.Forms.TextBox();
            this.lbCommission = new System.Windows.Forms.Label();
            this.lbAmountOfSales = new System.Windows.Forms.Label();
            this.gbOperator = new System.Windows.Forms.GroupBox();
            this.txtRemunerationPerPiece = new System.Windows.Forms.TextBox();
            this.txtPiecesProduced = new System.Windows.Forms.TextBox();
            this.lbAmountOfPiece = new System.Windows.Forms.Label();
            this.lbPartsProduced = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbTypeOfEmployee = new System.Windows.Forms.ComboBox();
            this.dtpDateOfEntry = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBaseSalary = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtIdentification = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.gbResults = new System.Windows.Forms.GroupBox();
            this.txtAntiquity = new System.Windows.Forms.TextBox();
            this.txtNetEarnings = new System.Windows.Forms.TextBox();
            this.lbAntiquity = new System.Windows.Forms.Label();
            this.lbNetEarnings = new System.Windows.Forms.Label();
            this.panelHeader.SuspendLayout();
            this.gbAboutEmployee.SuspendLayout();
            this.gbAdministrative.SuspendLayout();
            this.gbSeller.SuspendLayout();
            this.gbOperator.SuspendLayout();
            this.gbResults.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbFormTitle
            // 
            this.lbFormTitle.AutoSize = true;
            this.lbFormTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFormTitle.Location = new System.Drawing.Point(12, 9);
            this.lbFormTitle.Name = "lbFormTitle";
            this.lbFormTitle.Size = new System.Drawing.Size(590, 39);
            this.lbFormTitle.TabIndex = 0;
            this.lbFormTitle.Text = "Calculadora de Salario por Empleado";
            // 
            // panelHeader
            // 
            this.panelHeader.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panelHeader.Controls.Add(this.lbFormTitle);
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader.Location = new System.Drawing.Point(0, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(690, 59);
            this.panelHeader.TabIndex = 0;
            // 
            // gbAboutEmployee
            // 
            this.gbAboutEmployee.Controls.Add(this.btCalculate);
            this.gbAboutEmployee.Controls.Add(this.btClear);
            this.gbAboutEmployee.Controls.Add(this.gbAdministrative);
            this.gbAboutEmployee.Controls.Add(this.gbSeller);
            this.gbAboutEmployee.Controls.Add(this.gbOperator);
            this.gbAboutEmployee.Controls.Add(this.label5);
            this.gbAboutEmployee.Controls.Add(this.cmbTypeOfEmployee);
            this.gbAboutEmployee.Controls.Add(this.dtpDateOfEntry);
            this.gbAboutEmployee.Controls.Add(this.label4);
            this.gbAboutEmployee.Controls.Add(this.label3);
            this.gbAboutEmployee.Controls.Add(this.txtBaseSalary);
            this.gbAboutEmployee.Controls.Add(this.txtName);
            this.gbAboutEmployee.Controls.Add(this.txtIdentification);
            this.gbAboutEmployee.Controls.Add(this.label2);
            this.gbAboutEmployee.Controls.Add(this.label1);
            this.gbAboutEmployee.Location = new System.Drawing.Point(12, 65);
            this.gbAboutEmployee.Name = "gbAboutEmployee";
            this.gbAboutEmployee.Size = new System.Drawing.Size(666, 267);
            this.gbAboutEmployee.TabIndex = 1;
            this.gbAboutEmployee.TabStop = false;
            this.gbAboutEmployee.Text = "Datos del empleado";
            // 
            // btCalculate
            // 
            this.btCalculate.Image = global::Employees.Properties.Resources.dialog_ok_2;
            this.btCalculate.Location = new System.Drawing.Point(118, 170);
            this.btCalculate.Name = "btCalculate";
            this.btCalculate.Size = new System.Drawing.Size(100, 80);
            this.btCalculate.TabIndex = 13;
            this.btCalculate.Text = "Calcular";
            this.btCalculate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btCalculate.UseVisualStyleBackColor = true;
            this.btCalculate.Click += new System.EventHandler(this.btCalculate_Click);
            // 
            // btClear
            // 
            this.btClear.Image = global::Employees.Properties.Resources.edit_clear_3;
            this.btClear.Location = new System.Drawing.Point(224, 170);
            this.btClear.Name = "btClear";
            this.btClear.Size = new System.Drawing.Size(100, 80);
            this.btClear.TabIndex = 14;
            this.btClear.Text = "Limpiar";
            this.btClear.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btClear.UseVisualStyleBackColor = true;
            this.btClear.Click += new System.EventHandler(this.btClear_Click);
            // 
            // gbAdministrative
            // 
            this.gbAdministrative.Controls.Add(this.txtOvertime);
            this.gbAdministrative.Controls.Add(this.lbOvertime);
            this.gbAdministrative.Location = new System.Drawing.Point(351, 198);
            this.gbAdministrative.Name = "gbAdministrative";
            this.gbAdministrative.Size = new System.Drawing.Size(299, 52);
            this.gbAdministrative.TabIndex = 12;
            this.gbAdministrative.TabStop = false;
            this.gbAdministrative.Text = "Administrador";
            // 
            // txtOvertime
            // 
            this.txtOvertime.Location = new System.Drawing.Point(177, 19);
            this.txtOvertime.Name = "txtOvertime";
            this.txtOvertime.Size = new System.Drawing.Size(107, 20);
            this.txtOvertime.TabIndex = 1;
            // 
            // lbOvertime
            // 
            this.lbOvertime.AutoSize = true;
            this.lbOvertime.Location = new System.Drawing.Point(19, 22);
            this.lbOvertime.Name = "lbOvertime";
            this.lbOvertime.Size = new System.Drawing.Size(61, 13);
            this.lbOvertime.TabIndex = 0;
            this.lbOvertime.Text = "Horas extra";
            // 
            // gbSeller
            // 
            this.gbSeller.Controls.Add(this.txtCommission);
            this.gbSeller.Controls.Add(this.txtAmountOfSales);
            this.gbSeller.Controls.Add(this.lbCommission);
            this.gbSeller.Controls.Add(this.lbAmountOfSales);
            this.gbSeller.Location = new System.Drawing.Point(351, 112);
            this.gbSeller.Name = "gbSeller";
            this.gbSeller.Size = new System.Drawing.Size(299, 80);
            this.gbSeller.TabIndex = 11;
            this.gbSeller.TabStop = false;
            this.gbSeller.Text = "Vendedor";
            // 
            // txtCommission
            // 
            this.txtCommission.Location = new System.Drawing.Point(177, 45);
            this.txtCommission.Name = "txtCommission";
            this.txtCommission.Size = new System.Drawing.Size(107, 20);
            this.txtCommission.TabIndex = 3;
            // 
            // txtAmountOfSales
            // 
            this.txtAmountOfSales.Location = new System.Drawing.Point(177, 19);
            this.txtAmountOfSales.Name = "txtAmountOfSales";
            this.txtAmountOfSales.Size = new System.Drawing.Size(107, 20);
            this.txtAmountOfSales.TabIndex = 1;
            // 
            // lbCommission
            // 
            this.lbCommission.AutoSize = true;
            this.lbCommission.Location = new System.Drawing.Point(19, 48);
            this.lbCommission.Name = "lbCommission";
            this.lbCommission.Size = new System.Drawing.Size(74, 13);
            this.lbCommission.TabIndex = 2;
            this.lbCommission.Text = "% de comisión";
            // 
            // lbAmountOfSales
            // 
            this.lbAmountOfSales.AutoSize = true;
            this.lbAmountOfSales.Location = new System.Drawing.Point(19, 22);
            this.lbAmountOfSales.Name = "lbAmountOfSales";
            this.lbAmountOfSales.Size = new System.Drawing.Size(78, 13);
            this.lbAmountOfSales.TabIndex = 0;
            this.lbAmountOfSales.Text = "Monto vendido";
            // 
            // gbOperator
            // 
            this.gbOperator.Controls.Add(this.txtRemunerationPerPiece);
            this.gbOperator.Controls.Add(this.txtPiecesProduced);
            this.gbOperator.Controls.Add(this.lbAmountOfPiece);
            this.gbOperator.Controls.Add(this.lbPartsProduced);
            this.gbOperator.Location = new System.Drawing.Point(351, 26);
            this.gbOperator.Name = "gbOperator";
            this.gbOperator.Size = new System.Drawing.Size(299, 80);
            this.gbOperator.TabIndex = 10;
            this.gbOperator.TabStop = false;
            this.gbOperator.Text = "Operador";
            // 
            // txtRemunerationPerPiece
            // 
            this.txtRemunerationPerPiece.Location = new System.Drawing.Point(177, 45);
            this.txtRemunerationPerPiece.Name = "txtRemunerationPerPiece";
            this.txtRemunerationPerPiece.Size = new System.Drawing.Size(107, 20);
            this.txtRemunerationPerPiece.TabIndex = 3;
            // 
            // txtPiecesProduced
            // 
            this.txtPiecesProduced.Location = new System.Drawing.Point(177, 19);
            this.txtPiecesProduced.Name = "txtPiecesProduced";
            this.txtPiecesProduced.Size = new System.Drawing.Size(107, 20);
            this.txtPiecesProduced.TabIndex = 1;
            // 
            // lbAmountOfPiece
            // 
            this.lbAmountOfPiece.AutoSize = true;
            this.lbAmountOfPiece.Location = new System.Drawing.Point(19, 48);
            this.lbAmountOfPiece.Name = "lbAmountOfPiece";
            this.lbAmountOfPiece.Size = new System.Drawing.Size(83, 13);
            this.lbAmountOfPiece.TabIndex = 2;
            this.lbAmountOfPiece.Text = "Monto por pieza";
            // 
            // lbPartsProduced
            // 
            this.lbPartsProduced.AutoSize = true;
            this.lbPartsProduced.Location = new System.Drawing.Point(19, 22);
            this.lbPartsProduced.Name = "lbPartsProduced";
            this.lbPartsProduced.Size = new System.Drawing.Size(152, 13);
            this.lbPartsProduced.TabIndex = 0;
            this.lbPartsProduced.Text = "Cantidad de piezas producidas";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Tipo de empleado";
            // 
            // cmbTypeOfEmployee
            // 
            this.cmbTypeOfEmployee.FormattingEnabled = true;
            this.cmbTypeOfEmployee.Location = new System.Drawing.Point(120, 26);
            this.cmbTypeOfEmployee.Name = "cmbTypeOfEmployee";
            this.cmbTypeOfEmployee.Size = new System.Drawing.Size(204, 21);
            this.cmbTypeOfEmployee.TabIndex = 1;
            this.cmbTypeOfEmployee.SelectedValueChanged += new System.EventHandler(this.cmbTypeOfEmployee_SelectedValueChanged);
            // 
            // dtpDateOfEntry
            // 
            this.dtpDateOfEntry.Location = new System.Drawing.Point(120, 131);
            this.dtpDateOfEntry.Name = "dtpDateOfEntry";
            this.dtpDateOfEntry.Size = new System.Drawing.Size(204, 20);
            this.dtpDateOfEntry.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Fecha de Ingreso";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Salario Base";
            // 
            // txtBaseSalary
            // 
            this.txtBaseSalary.Location = new System.Drawing.Point(120, 105);
            this.txtBaseSalary.Name = "txtBaseSalary";
            this.txtBaseSalary.Size = new System.Drawing.Size(204, 20);
            this.txtBaseSalary.TabIndex = 7;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(120, 79);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(204, 20);
            this.txtName.TabIndex = 5;
            // 
            // txtIdentification
            // 
            this.txtIdentification.Location = new System.Drawing.Point(120, 53);
            this.txtIdentification.Name = "txtIdentification";
            this.txtIdentification.Size = new System.Drawing.Size(204, 20);
            this.txtIdentification.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Nombre";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Identificación";
            // 
            // gbResults
            // 
            this.gbResults.Controls.Add(this.txtAntiquity);
            this.gbResults.Controls.Add(this.txtNetEarnings);
            this.gbResults.Controls.Add(this.lbAntiquity);
            this.gbResults.Controls.Add(this.lbNetEarnings);
            this.gbResults.Location = new System.Drawing.Point(12, 338);
            this.gbResults.Name = "gbResults";
            this.gbResults.Size = new System.Drawing.Size(666, 122);
            this.gbResults.TabIndex = 2;
            this.gbResults.TabStop = false;
            this.gbResults.Text = "Resultado";
            // 
            // txtAntiquity
            // 
            this.txtAntiquity.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAntiquity.Location = new System.Drawing.Point(183, 69);
            this.txtAntiquity.Name = "txtAntiquity";
            this.txtAntiquity.ReadOnly = true;
            this.txtAntiquity.Size = new System.Drawing.Size(452, 35);
            this.txtAntiquity.TabIndex = 3;
            // 
            // txtNetEarnings
            // 
            this.txtNetEarnings.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNetEarnings.Location = new System.Drawing.Point(183, 28);
            this.txtNetEarnings.Name = "txtNetEarnings";
            this.txtNetEarnings.ReadOnly = true;
            this.txtNetEarnings.Size = new System.Drawing.Size(452, 35);
            this.txtNetEarnings.TabIndex = 1;
            // 
            // lbAntiquity
            // 
            this.lbAntiquity.AutoSize = true;
            this.lbAntiquity.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAntiquity.Location = new System.Drawing.Point(22, 75);
            this.lbAntiquity.Name = "lbAntiquity";
            this.lbAntiquity.Size = new System.Drawing.Size(121, 25);
            this.lbAntiquity.TabIndex = 2;
            this.lbAntiquity.Text = "Antigüedad";
            // 
            // lbNetEarnings
            // 
            this.lbNetEarnings.AutoSize = true;
            this.lbNetEarnings.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNetEarnings.Location = new System.Drawing.Point(22, 34);
            this.lbNetEarnings.Name = "lbNetEarnings";
            this.lbNetEarnings.Size = new System.Drawing.Size(127, 25);
            this.lbNetEarnings.TabIndex = 0;
            this.lbNetEarnings.Text = "Salario neto";
            // 
            // EmployeeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(690, 471);
            this.Controls.Add(this.gbResults);
            this.Controls.Add(this.gbAboutEmployee);
            this.Controls.Add(this.panelHeader);
            this.Name = "EmployeeForm";
            this.Text = "Calculadora de Salario";
            this.panelHeader.ResumeLayout(false);
            this.panelHeader.PerformLayout();
            this.gbAboutEmployee.ResumeLayout(false);
            this.gbAboutEmployee.PerformLayout();
            this.gbAdministrative.ResumeLayout(false);
            this.gbAdministrative.PerformLayout();
            this.gbSeller.ResumeLayout(false);
            this.gbSeller.PerformLayout();
            this.gbOperator.ResumeLayout(false);
            this.gbOperator.PerformLayout();
            this.gbResults.ResumeLayout(false);
            this.gbResults.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbFormTitle;
        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.GroupBox gbAboutEmployee;
        private System.Windows.Forms.GroupBox gbOperator;
        private System.Windows.Forms.TextBox txtRemunerationPerPiece;
        private System.Windows.Forms.TextBox txtPiecesProduced;
        private System.Windows.Forms.Label lbAmountOfPiece;
        private System.Windows.Forms.Label lbPartsProduced;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbTypeOfEmployee;
        private System.Windows.Forms.DateTimePicker dtpDateOfEntry;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBaseSalary;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtIdentification;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btCalculate;
        private System.Windows.Forms.Button btClear;
        private System.Windows.Forms.GroupBox gbAdministrative;
        private System.Windows.Forms.TextBox txtOvertime;
        private System.Windows.Forms.Label lbOvertime;
        private System.Windows.Forms.GroupBox gbSeller;
        private System.Windows.Forms.TextBox txtCommission;
        private System.Windows.Forms.TextBox txtAmountOfSales;
        private System.Windows.Forms.Label lbCommission;
        private System.Windows.Forms.Label lbAmountOfSales;
        private System.Windows.Forms.GroupBox gbResults;
        private System.Windows.Forms.TextBox txtAntiquity;
        private System.Windows.Forms.TextBox txtNetEarnings;
        private System.Windows.Forms.Label lbAntiquity;
        private System.Windows.Forms.Label lbNetEarnings;
    }
}