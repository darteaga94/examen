﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Employees.Classes;
using Employees.Models;

namespace Employees.Views
{
    public partial class EmployeeForm : Form
    {
        Dictionary<string, TypeOfEmployee> dictionary;

        public EmployeeForm()
        {
            InitializeComponent();

            //Se declara y llena un objeto Dictionary
            dictionary = new Dictionary<string, TypeOfEmployee>();
            dictionary.Add("Operador", TypeOfEmployee.Operator);
            dictionary.Add("Vendedor", TypeOfEmployee.Seller);
            dictionary.Add("Administrativo", TypeOfEmployee.Administrative);

            //Se inhabilitan los grupos de controles
            this.gbOperator.Enabled = false;
            this.gbSeller.Enabled = false;
            this.gbAdministrative.Enabled = false;

            //Se pega el combobox al origen de datos
            this.cmbTypeOfEmployee.DisplayMember = "Key";
            this.cmbTypeOfEmployee.ValueMember = "Value";
            this.cmbTypeOfEmployee.DataSource = new BindingSource(dictionary, null);
            this.cmbTypeOfEmployee.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void btCalculate_Click(object sender, EventArgs e)
        {
            //Declara una clase abstracta, pero se instanciará la que corresponde
            Employee employee;

            if ((cmbTypeOfEmployee.SelectedValue != null) && (txtIdentification.Text != string.Empty)
                && (txtName.Text != string.Empty) && (txtBaseSalary.Text != string.Empty))
            {
                try
                {
                    //La siguiente linea provocará una excepción si el dato no es numérico
                    Double.Parse(txtBaseSalary.Text);

                    switch ((TypeOfEmployee)cmbTypeOfEmployee.SelectedValue)
                    {
                        default:
                        case TypeOfEmployee.Operator:
                            //La siguiente linea provocará una excepción si el dato no es numérico
                            Double.Parse(txtPiecesProduced.Text);
                            Double.Parse(txtRemunerationPerPiece.Text);
                            

                            //Instancia en la variable "employee" un objeto "Operator"
                            employee = new Operator(
                                txtIdentification.Text,
                                txtName.Text,
                                dtpDateOfEntry.Value,
                                double.Parse(txtBaseSalary.Text));

                            //Se hace castign a "employee" para tener acceso a los métodos propios del objeto "Operator"
                            ((Operator)employee).PiecesProduced = int.Parse(txtPiecesProduced.Text);
                            ((Operator)employee).RemunerationPerPiece = double.Parse(txtRemunerationPerPiece.Text);
                            break;

                        case TypeOfEmployee.Seller:
                            //La siguiente linea provocará una excepción si el dato no es numérico
                            Double.Parse(txtAmountOfSales.Text);
                            Double.Parse(txtCommission.Text);

                            //Instancia en la variable "employee" un objeto "Seller"
                            employee = new Seller(
                                txtIdentification.Text,
                                txtName.Text,
                                dtpDateOfEntry.Value,
                                double.Parse(txtBaseSalary.Text));

                            //Se hace castign a "employee" para tener acceso a los métodos propios del objeto "Seller"
                            ((Seller)employee).AmountOfSales = double.Parse(txtAmountOfSales.Text);
                            ((Seller)employee).PercentageOfCommision = double.Parse(txtCommission.Text);
                            break;

                        case TypeOfEmployee.Administrative:
                            //La siguiente linea provocará una excepción si el dato no es numérico
                            Double.Parse(txtOvertime.Text);

                            //Instancia en la variable "employee" un objeto "Administrative"
                            employee = new Administrative(
                                txtIdentification.Text,
                                txtName.Text,
                                dtpDateOfEntry.Value,
                                double.Parse(txtBaseSalary.Text));

                            //Se hace castign a "employee" para tener acceso a los métodos propios del objeto "Administrative"
                            ((Administrative)employee).Overtime = int.Parse(txtOvertime.Text);
                            break;
                    }

                    //Haber declarado el objeto base "employee" en lugar de una clase específica evitó repetir
                    //estas linea en cada sección del switch-case
                    txtNetEarnings.Text = employee.CalculateSalary().ToString();
                    txtAntiquity.Text = employee.CalculateAtiquity().ToString();
                }
                catch (Exception)
                {
                    MessageBox.Show("Se esperaba un valor numérico","Error",MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
                MessageBox.Show("Debe llenar todos los datos de entrada","Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void btClear_Click(object sender, EventArgs e)
        {
            txtIdentification.Text = string.Empty;
            txtName.Text = string.Empty;
            txtBaseSalary.Text = string.Empty;
            dtpDateOfEntry.Value = DateTime.Today;
            cmbTypeOfEmployee.SelectedIndex = 0;
            this.ClearControlsOfGroups();
            cmbTypeOfEmployee.Focus();
        }

        private void cmbTypeOfEmployee_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cmbTypeOfEmployee.SelectedValue != null)
            {
                this.ClearControlsOfGroups();

                switch ((TypeOfEmployee)cmbTypeOfEmployee.SelectedValue)
                {
                    case TypeOfEmployee.Operator:
                        this.gbOperator.Enabled = true;
                        this.gbSeller.Enabled = false;
                        this.gbAdministrative.Enabled = false;
                        break;
                    case TypeOfEmployee.Seller:
                        this.gbOperator.Enabled = false;
                        this.gbSeller.Enabled = true;
                        this.gbAdministrative.Enabled = false;
                        break;
                    case TypeOfEmployee.Administrative:
                        this.gbOperator.Enabled = false;
                        this.gbSeller.Enabled = false;
                        this.gbAdministrative.Enabled = true;
                        break;
                }
            }
        }

        private void ClearControlsOfGroups()
        {
            //Operador
            this.txtPiecesProduced.Text = string.Empty;
            this.txtRemunerationPerPiece.Text = string.Empty;
            
            //Vendedor
            this.txtAmountOfSales.Text = string.Empty;
            this.txtCommission.Text = string.Empty;

            //Administrador
            this.txtOvertime.Text = string.Empty;
        }

    
    }
}
