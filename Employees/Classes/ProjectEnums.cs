﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employees.Classes
{
    public enum TypeOfEmployee
    {
        /// <summary>
        /// Al operador le pagan por piezas producidas
        /// </summary>
        Operator,
        /// <summary>
        /// Al vendedor le pagan una comisión sobre el monto de ventas
        /// </summary>
        Seller,
        /// <summary>
        /// Al administrativo le pagan horas extra
        /// </summary>
        Administrative
    }
}
