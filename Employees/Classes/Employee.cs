﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employees.Classes
{
    abstract class Employee
    {
        protected String _identification;
        protected String _name;
        protected DateTime _dateOfEntry;
        protected Double _baseSalary;
        protected const Double PERCENTAGE_ANNUITY = 3;

        public Employee()
        {

        }

        /// <summary>
        /// Construye una nueva instancia de Empleado
        /// </summary>
        /// <param name="identification">Identificación del empleado</param>
        /// <param name="name">Nombre del empleado</param>
        /// <param name="dateOfEntry">Fecha de ingreso</param>
        /// <param name="baseSalary">Salario base</param>
        public Employee(string identification, string name,
            DateTime dateOfEntry, double baseSalary)
        {
            this._identification = identification;
            this._name = name;
            this._dateOfEntry = dateOfEntry;
            this._baseSalary = baseSalary;
        }

        /// <summary>
        /// Calcula la antiguedad del empleado
        /// </summary>
        /// <returns>Cantidad de años trabajados</returns>
        public int CalculateAtiquity()
        {
            return (int)((DateTime.Today - this._dateOfEntry.Date).Days / 365);
        }

        /// <summary>
        /// Calcula el monto ganado por concepto de anualidades (años trabajados)
        /// </summary>
        /// <returns>Monto por anualidades</returns>
        public double CalculateAnnuity()
        {
            return (this._baseSalary * this.CalculateAtiquity() * (PERCENTAGE_ANNUITY/100));
        }

        /// <summary>
        /// Calcula el salario neto del empleado
        /// </summary>
        /// <returns>Salario neto</returns>
        public abstract double CalculateSalary();
    }
}
