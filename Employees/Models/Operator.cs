﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Employees.Classes;

namespace Employees.Models
{
    class Operator : Employee
    {
        private int piecesProduced;
        private double remunerationPerPiece;

        /// <summary>
        /// Crea una nueva instancia de Operador
        /// </summary>
        /// <param name="identification">Identificación del empleado</param>
        /// <param name="name">Nombre del empleado</param>
        /// <param name="dateOfEntry">Fecha de ingreso</param>
        /// <param name="baseSalary">Salario base</param>
        public Operator(string identification, string name,
            DateTime dateOfEntry, double baseSalary) :
            base(identification, name, dateOfEntry, baseSalary)
        {

        }

        /// <summary>
        /// Calcula el salario neto, sumando el salario base + monto de anualidades + monto por piezas producidas
        /// </summary>
        /// <returns>Salario neto del operador</returns>
        public override double CalculateSalary()
        {
            double salary = _baseSalary + CalculateAnnuity();
            salary += (this.piecesProduced * this.remunerationPerPiece);
            
            //Se retorna el salario redondeado a dos decimales
            return Math.Round(salary, 2);
        }

        /// <summary>
        /// Lee o asigna la cantidad de piezas producidas
        /// </summary>
        public int PiecesProduced
        {
            get { return piecesProduced; }
            set { piecesProduced = value; }
        }

        /// <summary>
        /// Lee o asigna el monto por remuneración por piezas producidas
        /// </summary>
        public double RemunerationPerPiece
        {
            get { return remunerationPerPiece; }
            set { remunerationPerPiece = value; }
        }
    }
}
