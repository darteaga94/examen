﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Employees.Classes;

namespace Employees.Models
{
    class Seller : Employee
    {
        private double amountOfSales;
        private double percentageOfCommision;

        /// <summary>
        /// Crea una nueva instancia de Vendedor
        /// </summary>
        /// <param name="identification">Identificación del empleado</param>
        /// <param name="name">Nombre del empleado</param>
        /// <param name="dateOfEntry">Fecha de ingreso</param>
        /// <param name="baseSalary">Salario base</param>
        public Seller(string identification, string name, 
            DateTime dateOfEntry, double baseSalary) : 
            base (identification, name, dateOfEntry, baseSalary)
        {

        }

        /// <summary>
        /// Calcula el salario neto, sumando el salario base + monto de anualidades + monto por comisiones de ventas
        /// </summary>
        /// <returns>Salario neto del operador</returns>
        public override double CalculateSalary()
        {
            double salary = _baseSalary + CalculateAnnuity();
            salary += (this.amountOfSales * (this.percentageOfCommision/100));

            //Se retorna el salario redondeado a dos decimales
            return Math.Round(salary, 2);
        }

        /// <summary>
        /// Lee o asigna el monto de las ventas
        /// </summary>
        public double AmountOfSales
        {
            get { return amountOfSales; }
            set { amountOfSales = value; }
        }

        /// <summary>
        /// Lee o asigna el porcentaje de comisión
        /// </summary>
        public double PercentageOfCommision
        {
            get { return percentageOfCommision; }
            set { percentageOfCommision = value; }
        }

    }
}