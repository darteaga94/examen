﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Employees.Classes;

namespace Employees.Models
{
    class Administrative : Employee
    {
        private double overtime;

        /// <summary>
        /// Crea una nueva instancia de Administrador
        /// </summary>
        /// <param name="identification">Identificación del empleado</param>
        /// <param name="name">Nombre del empleado</param>
        /// <param name="dateOfEntry">Fecha de ingreso</param>
        /// <param name="baseSalary">Salario base</param>
        public Administrative(string identification, string name,
            DateTime dateOfEntry, double baseSalary) :
            base(identification, name, dateOfEntry, baseSalary)
        {

        }

        /// <summary>
        /// Calcula el salario neto, sumando el salario base + monto de anualidades + monto por horas extras
        /// </summary>
        /// <returns>Salario neto del operador</returns>
        public override double CalculateSalary()
        {
            double salary = _baseSalary + CalculateAnnuity();
            double salaryByHour = (salary/4.33/48);
            salary += (salaryByHour*1.5*this.overtime);

            //Se retorna el salario redondeado a dos decimales
            return Math.Round(salary, 2);
        }

        /// <summary>
        /// Lee o asigna las cantidad de horas extra
        /// </summary>
        public double Overtime
        {
            get { return overtime; }
            set { overtime = value; }
        }
    }
}
